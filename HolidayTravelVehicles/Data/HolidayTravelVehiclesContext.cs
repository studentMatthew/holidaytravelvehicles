﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HolidayTravelVehicles.Models;

namespace HolidayTravelVehicles.Models
{
    public class HolidayTravelVehiclesContext : DbContext
    {
        public HolidayTravelVehiclesContext (DbContextOptions<HolidayTravelVehiclesContext> options)
            : base(options)
        {
        }

        public DbSet<HolidayTravelVehicles.Models.Customer> Customers { get; set; } 

        public DbSet<HolidayTravelVehicles.Models.Sale> Sale { get; set; }

        public DbSet<HolidayTravelVehicles.Models.Car> Car { get; set; }

        public DbSet<HolidayTravelVehicles.Models.SalesPerson> SalesPerson { get; set; }

        public DbSet<HolidayTravelVehicles.Models.Caravan> Caravan { get; set; }
    }
}
