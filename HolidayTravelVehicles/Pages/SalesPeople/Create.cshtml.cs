﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using HolidayTravelVehicles.Models;

namespace HolidayTravelVehicles.Pages.SalesPeople
{
    public class CreateModel : PageModel
    {
        private readonly HolidayTravelVehicles.Models.HolidayTravelVehiclesContext _context;

        public CreateModel(HolidayTravelVehicles.Models.HolidayTravelVehiclesContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public SalesPerson SalesPerson { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.SalesPerson.Add(SalesPerson);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}