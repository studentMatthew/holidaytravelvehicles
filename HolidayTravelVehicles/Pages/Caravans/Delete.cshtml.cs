﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using HolidayTravelVehicles.Models;

namespace HolidayTravelVehicles.Pages.Caravans
{
    public class DeleteModel : PageModel
    {
        private readonly HolidayTravelVehicles.Models.HolidayTravelVehiclesContext _context;

        public DeleteModel(HolidayTravelVehicles.Models.HolidayTravelVehiclesContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Caravan Caravan { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Caravan = await _context.Caravan.FirstOrDefaultAsync(m => m.ID == id);

            if (Caravan == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Caravan = await _context.Caravan.FindAsync(id);

            if (Caravan != null)
            {
                _context.Caravan.Remove(Caravan);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
