﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using HolidayTravelVehicles.Models;

namespace HolidayTravelVehicles.Pages.Sales
{
    public class CreateModel : PageModel
    {
        private readonly HolidayTravelVehicles.Models.HolidayTravelVehiclesContext _context;

        public CreateModel(HolidayTravelVehicles.Models.HolidayTravelVehiclesContext context)
        {
            _context = context;
        }


        [BindProperty]
        public Sale Sale { get; set; }
        public List<Customer> Customers { get; set; }

        public List<Car> Cars { get; set;}
        public List<Caravan> Caravans { get; set; }

        public List<SalesPerson> SalesPeople { get; set; }

        public IActionResult OnGet()
        {
            Customers = _context.Customers
                        .OrderBy(c => c.Name)
                        .ToList();

            Cars = _context.Car
                        .OrderBy(c => c.Name)
                        .ToList();

            Caravans = _context.Caravan
                        .OrderBy(c => c.Name)
                        .ToList();

            SalesPeople = _context.SalesPerson
                            .OrderBy(s => s.Name)
                            .ToList();
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Sale.Add(Sale);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}