﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using HolidayTravelVehicles.Models;

namespace HolidayTravelVehicles.Pages.Sales
{
    public class DetailsModel : PageModel
    {
        private readonly HolidayTravelVehicles.Models.HolidayTravelVehiclesContext _context;

        public DetailsModel(HolidayTravelVehicles.Models.HolidayTravelVehiclesContext context)
        {
            _context = context;
        }

        public Sale Sale { get; set; }

        public Customer Customer { get; set; }
        public Car Car { get; set; }
        public Caravan Caravan { get; set; }
        public SalesPerson SalesPerson { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Sale = await _context.Sale.FirstOrDefaultAsync(m => m.ID == id);
            Customer = await _context.Customers.FirstOrDefaultAsync(m => m.ID == Sale.CustomerId);

            Car = await _context.Car.FirstOrDefaultAsync(m => m.ID == Sale.CarsId);

            Caravan = await _context.Caravan.FirstOrDefaultAsync(m => m.ID == Sale.CaravanId);

            SalesPerson = await _context.SalesPerson.FirstOrDefaultAsync(m => m.ID == Sale.SalesPersonId);

            if (Sale == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
