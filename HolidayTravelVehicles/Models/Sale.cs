﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayTravelVehicles.Models
{
    public class Sale
    {
        public int ID { get; set; }

        public decimal Cost { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("Car")]
        public int? CarsId { get; set; }

        [ForeignKey("Caravan")]
        public int? CaravanId { get; set; }

        [ForeignKey("SalesPerson")]
        public int? SalesPersonId { get; set; }

    }
}
