﻿using System.Collections.Generic;

namespace HolidayTravelVehicles.Models
{
    public class Inventory
    {
        public int ID { get; set; }

        public IEnumerable<Caravan> Caravans { get; set; }

        public IEnumerable<Car> Cars { get; set; }
    }
}