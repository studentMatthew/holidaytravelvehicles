﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayTravelVehicles.Models
{
    public interface IReport
    {
        int ID { get; set; }
        string Name { get; set; }

        [DataType(DataType.Date)]
        DateTime DateCreated { get; set; }
        
    }
    public class SalesReport: IReport
    {
        public int ID { get; set; }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        [ForeignKey("Car")]
        public int SaleId { get; set; }

    }

    public class StockReport : IReport
    {
        public int ID { get; set; }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        public Inventory Inventory { get; set; }

    }
}
