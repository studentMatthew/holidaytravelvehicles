﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayTravelVehicles.Models
{
    interface IVehicle
    {
        int ID { get; set; }
        string Name { get; set; }

        string Type { get; set; }

        int Cost { get; set; }
        
    }


    public partial class Car : IVehicle
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public int Cost { get; set; }

        public string Brand { get; set; }
        public string Engine { get; set; }
    }

    public partial class Caravan : IVehicle
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public int Cost { get; set; }

        public string Brand { get; set; }

        public string Size { get; set; }
    }



}
