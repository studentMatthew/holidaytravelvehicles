﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayTravelVehicles.Models
{
    public class Customer
    {
        public int ID { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Birthday Day")]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string PhoneNumber { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Address { get; set; }

    }
}
