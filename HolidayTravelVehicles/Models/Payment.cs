﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayTravelVehicles.Models
{
    public class Payment
    {
        public int ID { get; set; }

        public int Cost { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

    }
}
