﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HolidayTravelVehicles.Models
{
    interface Booking
    {
        int ID { get; set; }

        [DataType(DataType.Date)]
        DateTime? BookingTime { get; set; }

        [ForeignKey("Customer")]
        int CustomerId { get; set; }

        [ForeignKey("SalesPerson")]
        int SalePersonId { get; set; }
    }

    public partial class TestDrive : Booking
    {
        public int ID { get; set; }
        public DateTime? BookingTime { get; set; }

    
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("SalesPerson")]
        public int SalePersonId { get; set; }

        [ForeignKey("Car")]
        public int CarID { get; set; }

    }

    public partial class Service : Booking
    {
        public int ID { get; set; }
        public DateTime? BookingTime { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("SalesPerson")]
        public int SalePersonId { get; set; }

    }
        
}
